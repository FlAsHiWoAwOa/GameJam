﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTNListener : MonoBehaviour {

    [SerializeField]
    private GlobuleFactory factory;
    [SerializeField]
    private string type;

    public string Type { get; set; }
    [SerializeField]
    private SpriteRenderer baseRenderer;
    [SerializeField]
    private SpriteRenderer selectedRenderer;

    private bool running;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Deselect() {
        selectedRenderer.enabled = false;
        baseRenderer.enabled = true;
    }
    public void EnableBtn(bool res) {
        running = res;
        if(!running)Deselect();
    }
    private void OnMouseDown() {
        if (running) {
            factory.SetTypeGlobul(type);
            selectedRenderer.enabled = true;
            baseRenderer.enabled = false;
        }
       
    }
}
