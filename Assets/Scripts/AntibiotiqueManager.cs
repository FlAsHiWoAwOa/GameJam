﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntibiotiqueManager: MonoBehaviour {


    [SerializeField]
    private float tauxIdealPills;
    [SerializeField]
    private int nbpills;
    [SerializeField]
    private GameManager GameManage;
    [SerializeField]
    private float tauxActuel;

    [SerializeField]
    private PrescriptionPop prescription;
   
   
    // Use this for initialization
    void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {
        tauxActuel = GameManage.TauxActuelInfection;
	}

    public void UsePills()
    {
        if(nbpills >= 1)
        {
            float tauxEfficacite = 100 - Mathf.Abs(tauxIdealPills - tauxActuel);
            --nbpills;
            GameManage.usePills(tauxEfficacite);
        }
        

    }

    public void showPrescription(bool actif)
    {
        
       
        prescription.enabled = actif;
        prescription.gameObject.SetActive(actif);
    }
}
