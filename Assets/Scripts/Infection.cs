﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Infection : MonoBehaviour {


    
    private string killedBy;
    [SerializeField]
    private InfectedOrgane[] organeTouched;

    public string KilledBy { get; set; }
    public InfectedOrgane[] OrganeTouched { get { return organeTouched; } set { organeTouched = value; } }

    // Use this for initialization
    void Start () {
        RandomTarget();
	}
	
    void RandomTarget() {
        int res = UnityEngine.Random.Range(0, 3);

        switch (res) {
            case 0:
                KilledBy = "A";
                break;
            case 1:
                KilledBy = "B";
                break;
            case 2:
                KilledBy = "C";
                break;
        }
    }


	// Update is called once per frame
	void Update () {
		
	}

}
