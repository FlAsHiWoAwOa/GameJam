﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    private float lerpSpeed;
    private float fileAmount;
    [SerializeField]
    private Image content;
    [SerializeField]
    private Text valueText;
    public float MaxValue { get; set; }
    public float Value {
        set {
            valueText.text = value+"%";
            if (value < 50) valueText.color = Color.white;
            else if (value < 75) valueText.color = Color.yellow;
            else valueText.color = Color.red;
            
            fileAmount = Map(value, 0, MaxValue, 0, 1);

        }
    }
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        HandleBar();
	}

    private float Map(float value,float inMin,float inMax,float outMin,float outMax) {
        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
    private void HandleBar() {
        if (fileAmount != content.fillAmount) {
            content.fillAmount = Mathf.Lerp(content.fillAmount, fileAmount, Time.deltaTime * lerpSpeed);
        }
        
    }
}
