﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobuleFactory : MonoBehaviour {

	// Use this for initialization

    
    
    [SerializeField]
    private Globule Prefabs;
    
    
    [SerializeField]
    private float TimeSpawn;

    [SerializeField]
    private BTNListener btnA;
    [SerializeField]
    private BTNListener btnB;
    [SerializeField]
    private BTNListener btnC;
    private InfectedOrgane target;
    private bool cooldown;
    private bool endOfRound;
    private float minus;
    public float Minus { get; set; }
    private Coroutine coroutine=null;
    void Start () {
       
        
        endOfRound = false;
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void StartRound() {
        btnA.EnableBtn(true);
        btnB.EnableBtn(true);
        btnC.EnableBtn(true);
        endOfRound = false;

    }
    public void StopRound() {
        btnA.EnableBtn(false);
        btnB.EnableBtn(false);
        btnC.EnableBtn(false);
        endOfRound = true;
        target = null;
        StopCoroutine(coroutine);
    }
   
    private IEnumerator SpawnGlobul() {
        yield return new WaitForSeconds(5.0f);
        while (!endOfRound) {
            if(target)target.SetGlobule(Prefabs, minus);
            yield return new WaitForSeconds(TimeSpawn);

        }
    }

    public void SetTarget(InfectedOrgane organe) {
        Debug.Log("test set target " + organe);
        if (target)target.spriteDisable();
        target = organe;
    }
    public void SetTypeGlobul(string type) {

       Prefabs.Type = type;
        
        switch (type) {
            case "A":
                btnB.Deselect();
                btnC.Deselect();
                break;
            case "B":
                btnA.Deselect();
                btnC.Deselect();
                break;
            case "C":
                btnB.Deselect();
                btnA.Deselect();
                break;

        }
      
        if (coroutine != null) StopCoroutine(coroutine);
        coroutine = null;
        
       coroutine= StartCoroutine(SpawnGlobul());
    }
   
}



