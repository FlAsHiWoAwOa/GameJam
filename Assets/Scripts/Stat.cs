﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Stat {
    [SerializeField]
    private BarScript bar;
    [SerializeField]
    private float maxVal;
    [SerializeField]
    private float currentValue;
    public float CurrentValue {

        get { return currentValue; }
        set {


            currentValue = Mathf.Clamp(value, 0, MaxVal);
            bar.Value = currentValue;
        }
    }

    public float MaxVal {
        get {
            return maxVal;
        }

        set {

            maxVal = value;
            bar.MaxValue = maxVal;
        }
    }
    public void Initialize() {
        MaxVal = maxVal;
        CurrentValue = currentValue;
    }
}
