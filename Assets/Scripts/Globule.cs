﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globule : MonoBehaviour {

    // Use this for initialization

    private string type;
   
    [SerializeField]
    private Sprite[] sprite;
    [SerializeField]
    private SpriteRenderer renderer;
    public string Type {
        get { return type; }
        set {
            type = value;
            switch (type) {
                case "A":
                    renderer.sprite = sprite[0];
                    break;
                case "B":
                    renderer.sprite = sprite[1];
                    break;
                case "C":
                    renderer.sprite = sprite[2];
                    break;
            }
        }
    }
	void Start () {
        
	}
	// Update is called once per frame
	void Update () {
		
	}
    private void FixedUpdate() {
        
    }
    
}
