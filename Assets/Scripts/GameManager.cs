﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [SerializeField]
    private InfectedOrgane[] organes;
    public InfectedOrgane[] Organes { get { return organes; } }
    [SerializeField]
    private AntibiotiqueManager antibiotiqueManager;

    [SerializeField]
    private float tauxActuelInfection;


    [SerializeField]
    private float tauxPopPrescription;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private GameObject gameOverPanel;

    [SerializeField]
    private Text gameOverScore;

    [SerializeField]
    private float scoreBySecond;

    private bool gameActive;

    public float TauxActuelInfection {
        get {
            return tauxActuelInfection;
        }

        set {

            tauxActuelInfection = value;
            scoreText.text = "" + Mathf.Round(tauxActuelInfection * 100) + "%";
        }
    }

    private void Start() {

        gameActive = true;
    }

    void Update() {
        float[] tab = new float[organes.Length];
        for (int i = 0; gameActive && i < organes.Length; i++) {
            Debug.Log("beforeIf" + organes[i].Dead);
            if (organes[i].Dead) {
                Debug.Log("dead" + organes[i].Dead);
                gameActive = false;
                LoseTheGame();
            }
            tab[i] = organes[i].Infection;
        }

        TauxActuelInfection = Mathf.Max(tab);

        if (TauxActuelInfection * 100 >= tauxPopPrescription) antibiotiqueManager.showPrescription(true);


    }

    private void LoseTheGame() {
        EndRound();
        if (gameOverPanel) {

            gameOverPanel.SetActive(true);
        }
    }

    public void usePills(float taux) {
        foreach (InfectedOrgane item in organes) {
            float tauxUsage = ((taux * tauxActuelInfection) / 100);
            item.Medicament(tauxUsage / 3); // diminue l'effet des antibio
        }
    }

    public void EndRound() {
        foreach (InfectedOrgane org in organes) {
            org.StopRound();
        }
        TauxActuelInfection = 0;
        antibiotiqueManager.showPrescription(false);
    }
    public void StartRound() {
        foreach (InfectedOrgane org in organes) {
            org.StartRound();
        }
    }

    public bool IsSafe() {
        bool res = true;
        foreach (InfectedOrgane org in organes) {
            if (res) res = org.Safe;

        }

        return res;
    }

    public void SetJoursSurvecus(int jours) {
        gameOverScore.text = ""+jours;
    }


}
