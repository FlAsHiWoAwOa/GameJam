﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    // Use this for initialization

    private int level;
    [SerializeField]
    private float TauxInfection;
    [SerializeField]
    private Text textLevel;
    
    [SerializeField]
    private GlobuleFactory factory;


    [SerializeField]
    private Infection[] listInfection;
    
    private int allsafe;
    [SerializeField]
    private AudioClip[] effectEnterLevel;
   [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private GameManager gameManager;
    
    public bool IsRunning { get; set; }
    void Start () {
        level = 0;
        LoadLevel();
        
	}
	
	// Update is called once per frame
	void Update () {
        bool res = gameManager.IsSafe();
        if (res) {
            OrganSafe();
        }
	}


    void LoadLevel() {
        AudioClip clip = effectEnterLevel[Random.Range(0, effectEnterLevel.Length)];
        audioSource.PlayOneShot(clip, audioSource.volume);
       
        if (level > 0) textLevel.text = "Jours " + level;
        
        Infection cur =listInfection[Random.Range(0, listInfection.Length)];

        foreach(InfectedOrgane org in cur.OrganeTouched) {
          
            org.InfectionCurr = cur;
            
            org.TAUXINFECTION_BASE = TauxInfection;
        }

        int rand = Random.Range(0, cur.OrganeTouched.Length);

        cur.OrganeTouched[rand].Infection = 0.2F;
        for(int i = 0; i < cur.OrganeTouched.Length; i++) {
            if (i != rand) cur.OrganeTouched[i].Infection = 0.1F;
        }


        factory.StartRound();
        gameManager.StartRound();
        IsRunning = true;
        GenerateTaux();
        
    }


    private void GenerateTaux() {
        if (level != 0) {
            if (level % 2 == 0) {
                TauxInfection += 0.015f;
            } else {
                TauxInfection -= 0.003f;
            }
        }
       
        
    }
    private void OrganSafe() {
      
            IsRunning = false;
            factory.StopRound();
          
            gameManager.EndRound();



           StartCoroutine(CoolDown());
            
        
        
        
    }


    private IEnumerator CoolDown() {
         yield return new WaitForSeconds(5.0f);
        level++;
        gameManager.SetJoursSurvecus(level);
        LoadLevel();
    }

    



}
