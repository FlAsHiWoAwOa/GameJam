﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[Serializable]
public class InfectedOrgane : MonoBehaviour {

    // Use this for initialization
   
    [SerializeField]
    public float TAUXINFECTION_BASE;

    [SerializeField]
    public float TAUXSOIN;

    private float infection;
   
   
    [SerializeField]
    private float TimeBetweenInfection;

    private bool dead;
    [SerializeField]
    public GlobuleFactory globuleFactory;
    [SerializeField]
    private SpriteRenderer spriteBase;
    [SerializeField]
    private SpriteRenderer spriteSelected;
    private Infection infectionCurr;
    [SerializeField]
    private LevelManager LevelManager;
    private bool safe;
    public bool Safe { get { return safe; } }
    public Infection InfectionCurr {
        get { return infectionCurr; }
        set {
            infectionCurr = value;
            EndOfRound = false;
            
           
        }

    }
    [SerializeField]
    private Stat infectionState;
    private bool endOfRound;
    public bool EndOfRound { get; set; }
    

    void Start () {
        infectionState.Initialize();
        //Infection = 0.9f;
        
    }
	public void StartRound() {
        EndOfRound = false;
        if (Infection <= 0) {
            safe = true;
           
        }

        StartCoroutine(Proliferer());
    }
    public void StopRound() {
        StopCoroutine("Proliferer");
        EndOfRound = true;
        spriteBase.enabled = true;
        spriteSelected.enabled = false;
        infectionCurr = null;
        safe = false;
        infectionState.CurrentValue = 0;
       
    }
    
	// Update is called once per frame
	void Update () {
       
    }
    private void FixedUpdate() {
       
    }

    
    private void OnMouseDown() {
        if (!EndOfRound) {
            globuleFactory.SetTarget(this);
            spriteSelected.enabled = true;
            spriteBase.enabled = false;
        }
    }
    
   public void spriteDisable() {
        spriteSelected.enabled = false;
        spriteBase.enabled = true;
    }

    private IEnumerator Proliferer() {

        while (!Dead && endOfRound==false && Infection>0) {
            yield return new WaitForSeconds(TimeBetweenInfection);
            Infecter();
        }
        

        
    }


   





    public float Infection {
        get {
            return infection;
        }

        set {
            infection = Mathf.Min(value, 1);
           
            infectionState.CurrentValue = (float)Math.Round(infection*100);
            
           
        }
    }

    
    public bool Dead {
        get {
            if (Infection >= 1) dead = true;
            return dead;
        }

        set {
            dead = value;
        }
    }

   



    public void SetGlobule(Globule globule,float minus=0) {
       
        if (infectionCurr!=null && infectionCurr.KilledBy.Equals(globule.Type)) {
            Infection = Infection -  (TAUXSOIN -minus) ;
        }
        if (!safe && Infection <= 0.01) {
            StopCoroutine("Proliferer");
            safe = true;
        }


    }

    
    public void Medicament(float taux) {
        Infection = Infection - taux;
    }

    public void Infecter() {
        if (InfectionCurr && !safe) {
            Infection = Infection + TAUXINFECTION_BASE;
        }
         
    }
}
