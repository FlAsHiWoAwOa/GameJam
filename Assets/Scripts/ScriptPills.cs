﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScriptPills : MonoBehaviour, IPointerClickHandler
{

    private Image img;
    [SerializeField]
    private AntibiotiqueManager antibio;
    
    // Use this for initialization
    void Start () {
        img =  GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        // if (Input.GetKeyDown(KeyCode.Space)) DesableImg(); doit être appelé dans la plaquette, définir la pillule à désactiver
    }

    public void OnPointerClick(PointerEventData eventData)
    {
       
        antibio.UsePills();
        DesableImg(false);

        
    }

    public void DesableImg(bool disable)
    {

        
        img.gameObject.SetActive(disable);
    }

}
