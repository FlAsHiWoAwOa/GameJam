﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrescriptionPop : MonoBehaviour {

    private bool actif;

    public bool Actif
    {
        get
        {
            return actif;
        }

        set
        {
            actif = value;
        }
    }


    // Use this for initialization
    void Start () {
        actif = false;
        enabled = false;
        this.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void showPrescription(bool actif)
    {
        this.actif = actif;
    }
}
